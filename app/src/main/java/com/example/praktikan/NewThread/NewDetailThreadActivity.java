package com.example.praktikan.NewThread;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.R;

public class NewDetailThreadActivity extends AppCompatActivity {
    String detailJudulBaru, detailKeteranganBaru;

    TextView DetJudul, DetKeterangan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_detail_thread);

        detailJudulBaru = getIntent().getStringExtra("judul_baru");
        detailKeteranganBaru = getIntent().getStringExtra("keterangan_baru");

        DetJudul = (TextView) findViewById(R.id.detail_judul_baru);
        DetKeterangan = (TextView) findViewById(R.id.detail_keterangan_baru);

        DetJudul.setText(detailJudulBaru);
        DetKeterangan.setText(detailKeteranganBaru);
    }
}
