package com.example.praktikan.NewThread;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.ApiService;
import com.example.praktikan.MainActivity;
import com.example.praktikan.R;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewThreadActivity extends AppCompatActivity {

    String namaMateri, id_materi;

    EditText judulBaru, keteranganBaru, materiBaru;

    Button btnBuat;

    int mahasiswa_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_form);

        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        mahasiswa_id = sharedPreferences.getInt("kirim_id_mahasiswa", 0);

        id_materi = getIntent().getStringExtra("id_materi");
        namaMateri = getIntent().getStringExtra("namaMateri");

        judulBaru = (EditText) findViewById(R.id.input_judul_baru);
        materiBaru = (EditText) findViewById(R.id.input_materi_baru);
        keteranganBaru = (EditText) findViewById(R.id.input_keterangan_baru);

        materiBaru.setText(namaMateri);

        btnBuat = (Button) findViewById(R.id.btnbuat);
        btnBuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lj = judulBaru.getText().toString().trim();
                String lk = keteranganBaru.getText().toString().trim();

                if (lj.length() < 5 ){
                    judulBaru.setError("Judul anda tidak boleh kurang dari 5 karakter");
                } else if (lk.length() < 10) {
                    keteranganBaru.setError("Keterangan anda tidak boleh kurang dari 10 karakter");
                } else {
                    create();
                }
            }
        });
    }

    public void create() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        Call<ResponseBody> call = service.saveThread(
                String.valueOf(mahasiswa_id),
                String.valueOf(id_materi),
                judulBaru.getText().toString().trim(),
                keteranganBaru.getText().toString().trim());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Intent intent = new Intent(NewThreadActivity.this, NewDetailThreadActivity.class);
                intent.putExtra("judul_baru", judulBaru.getText().toString());
                intent.putExtra("keterangan_baru", keteranganBaru.getText().toString());
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(NewThreadActivity.this, "Error" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
