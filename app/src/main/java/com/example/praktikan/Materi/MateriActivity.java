package com.example.praktikan.Materi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.ApiService;
import com.example.praktikan.MainActivity;
import com.example.praktikan.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MateriActivity extends AppCompatActivity {
    ArrayList<MateriModel> data_materi = new ArrayList<MateriModel>();
    ListView listview;
    MateriAdapter adapter;

    ProgressDialog loading;

    String id_modul;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi);

        id_modul = getIntent().getStringExtra("id_modul");

        listview = (ListView) findViewById(R.id.list_materi);
        listview.setDividerHeight(0);
    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(MateriActivity.this, null, "Please wait...", true, false);

        Call<List<MateriModel>> call = service.getMateri(id_modul);
        call.enqueue(new Callback<List<MateriModel>>() {
            @Override
            public void onResponse(Call<List<MateriModel>> call, Response<List<MateriModel>> response) {
                data_materi.clear();

                if (response.isSuccessful()){
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        MateriModel data = new MateriModel(
                                response.body().get(i).getNamaMateri(),
                                response.body().get(i).getId_materi()
                        );
                        data_materi.add(data);
                    }

                    adapter = new MateriAdapter(MateriActivity.this, R.layout.item_materi, data_materi);
                    listview.setAdapter(adapter);

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<MateriModel>> call, Throwable t) {
                Toast.makeText(MateriActivity.this, "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 ){
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        data_materi.clear();
        adapter = new MateriAdapter(MateriActivity.this, R.layout.item_materi, data_materi);
        listview.setAdapter(adapter);
        setup();
    }
}
