package com.example.praktikan.ThreadList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.praktikan.CustomOnItemClickListener;
import com.example.praktikan.DetailThread.DetailThreadActivity;
import com.example.praktikan.R;

import java.util.ArrayList;

public class ThreadListAdapter extends ArrayAdapter <ThreadListModel> {

    private ArrayList<ThreadListModel> list;
    private LayoutInflater inflater;
    private int res;

    public ThreadListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<ThreadListModel> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        MyHolder holder = null;

        if (convertView == null){
            convertView = inflater.inflate(res, parent, false);

            holder = new MyHolder();

            holder.Judul = (TextView) convertView.findViewById(R.id.list_judul_thread);
            holder.Keterangan = (TextView) convertView.findViewById(R.id.list_keterangan_thread);
            holder.Nama = (TextView) convertView.findViewById(R.id.list_nama_mahasiswa_thread);
            holder.ThreadList = (CardView) convertView.findViewById(R.id.cardview_thread_list);
            holder.batas = (View) convertView.findViewById(R.id.batas_thread);

            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }

//        SharedPreferences sp_status = ((AppCompatActivity) getContext()).getSharedPreferences("data_status_thread", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sp_status.edit();
//        editor.putInt("kirim_status_thread", list.get(position).getStatus());
//        editor.apply();

        holder.Nama.setText(list.get(position).getNamaMahasiswa());
        holder.Judul.setText(list.get(position).getJudulThread());
        holder.Keterangan.setText(list.get(position).getKet());
        holder.ThreadList.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                SharedPreferences sp_status = ((AppCompatActivity) getContext()).getSharedPreferences("data_status_thread", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp_status.edit();
                editor.putInt("kirim_status_thread", list.get(position).getStatus());
                editor.putInt("kirim_poster", list.get(position).getPoster());
                editor.apply();

                Intent intent = new Intent(context, DetailThreadActivity.class);
                intent.putExtra("idThread", String.valueOf(list.get(position).getIdThread()));
                intent.putExtra("judulThread", String.valueOf(list.get(position).getJudulThread()));
                intent.putExtra("ket", String.valueOf(list.get(position).getKet()));
                intent.putExtra("namaMahasiswa", String.valueOf(list.get(position).getNamaMahasiswa()));
                intent.putExtra("posted", String.valueOf(list.get(position).getPosted()));
                intent.putExtra("npm", String.valueOf(list.get(position).getNpm()));
                intent.putExtra("comment_counter", list.get(position).getComment());
                ((AppCompatActivity) context).startActivity(intent);

            }
        }));

        if (position == getCount()-1){
            holder.batas.setVisibility(View.GONE);
        } else {
            holder.batas.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(ThreadListModel object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder {
        TextView Judul;
        TextView Keterangan;
        TextView Nama;

        CardView ThreadList;

        View batas;
    }
}
