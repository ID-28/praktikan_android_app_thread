package com.example.praktikan.ThreadList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThreadListModel {
    @SerializedName("idThread")
    @Expose
    private int idThread;

    @SerializedName("judulThread")
    @Expose
    private String judulThread;

    @SerializedName("ket")
    @Expose
    private String ket;

    @SerializedName("posted")
    @Expose
    private String posted;

    @SerializedName("poster")
    @Expose
    private int poster;

    @SerializedName("namaMahasiswa")
    @Expose
    private String namaMahasiswa;

    @SerializedName("npm")
    @Expose
    private String npm;

    @SerializedName("comment")
    @Expose
    private int comment;

    @SerializedName("status")
    @Expose
    private int status;

    public ThreadListModel(int idThread, String judulThread, String ket, String namaMahasiswa, String posted, int poster, String npm, int comment, int status){
        this.idThread = idThread;
        this.judulThread = judulThread;
        this.ket = ket;
        this.namaMahasiswa = namaMahasiswa;
        this.posted = posted;
        this.poster = poster;
        this.npm = npm;
        this.comment = comment;
        this.status = status;
    }


    public int getIdThread() {
        return idThread;
    }

    public void setIdThread(int idThread) {
        this.idThread = idThread;
    }

    public String getJudulThread() {
        return judulThread;
    }

    public void setJudulThread(String judulThread) {
        this.judulThread = judulThread;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getPosted() {
        return posted;
    }

    public void setPosted(String posted) {
        this.posted = posted;
    }

    public String getNamaMahasiswa() {
        return namaMahasiswa;
    }

    public void setNamaMahasiswa(String namaMahasiswa) {
        this.namaMahasiswa = namaMahasiswa;
    }

    public int getPoster() {
        return poster;
    }

    public void setPoster(int poster) {
        this.poster = poster;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
