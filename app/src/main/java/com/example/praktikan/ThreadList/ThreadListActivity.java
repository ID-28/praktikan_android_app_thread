package com.example.praktikan.ThreadList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.ApiService;
import com.example.praktikan.MainActivity;
import com.example.praktikan.NewThread.NewThreadActivity;
import com.example.praktikan.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ThreadListActivity extends AppCompatActivity {

    ArrayList<ThreadListModel> data_thread = new ArrayList<ThreadListModel>();
    ListView listview;
    ThreadListAdapter adapter;

    FloatingActionButton FAB;

    TextView listKosong;

    ProgressDialog loading;

    String id_materi, namaMateri;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_list);

        listview = (ListView) findViewById(R.id.list_thread);
        listview.setDividerHeight(0);

        id_materi = getIntent().getStringExtra("id_materi");
        namaMateri = getIntent().getStringExtra("namaMateri");

        listKosong = (TextView) findViewById(R.id.thread_kosong);

        FAB = (FloatingActionButton) findViewById(R.id.fab);
        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ThreadListActivity.this, NewThreadActivity.class);
                intent.putExtra("id_materi", id_materi);
                intent.putExtra("namaMateri", namaMateri);
                startActivity(intent);
            }
        });
    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(ThreadListActivity.this, null, "Please wait...", true,  false);

        Call<List<ThreadListModel>> call = service.getThreadList(id_materi);
        call.enqueue(new Callback<List<ThreadListModel>>() {
            @Override
            public void onResponse(Call<List<ThreadListModel>> call, Response<List<ThreadListModel>> response) {
                data_thread.clear();

                if (response.isSuccessful()){
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        ThreadListModel data = new ThreadListModel(
                                response.body().get(i).getIdThread(),
                                response.body().get(i).getJudulThread(),
                                response.body().get(i).getKet(),
                                response.body().get(i).getNamaMahasiswa(),
                                response.body().get(i).getPosted(),
                                response.body().get(i).getPoster(),
                                response.body().get(i).getNpm(),
                                response.body().get(i).getComment(),
                                response.body().get(i).getStatus()
                        );

                        data_thread.add(data);
                    }

                    adapter = new ThreadListAdapter(ThreadListActivity.this, R.layout.item_thread_list, data_thread);
                    listview.setAdapter(adapter);

                    if (adapter.getCount() < 1 ){
                        listview.setVisibility(View.INVISIBLE);
                        listKosong.setVisibility(View.VISIBLE);
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<ThreadListModel>> call, Throwable t) {
                Toast.makeText(ThreadListActivity.this, "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        data_thread.clear();
        adapter = new ThreadListAdapter(ThreadListActivity.this, R.layout.item_thread_list, data_thread);
        listview.setAdapter(adapter);
        setup();
    }
}
