package com.example.praktikan.DetailThread;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.ApiService;
import com.example.praktikan.MainActivity;
import com.example.praktikan.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailThreadActivity extends AppCompatActivity {
    ProgressDialog loading;

    String idThread, judulThread, ket, namaMahasiswa, posted, poster, npm;

    TextView ThreadJudul, Nama, Ket, CC;

    ArrayList<DetailCommentModel> data_comment = new ArrayList<DetailCommentModel>();
    ListView listview;
    DetailCommentAdapter adapter;

    TextView komen_kosong;
    ImageView btnKomen;
    EditText inputKomen;

    int id_mahasiswa, status_thread, comment_counter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_thread);

        idThread = getIntent().getStringExtra("idThread");
        judulThread = getIntent().getStringExtra("judulThread");
        ket = getIntent().getStringExtra("ket");
        namaMahasiswa = getIntent().getStringExtra("namaMahasiswa");
        posted = getIntent().getStringExtra("posted");
        poster = getIntent().getStringExtra("poster");
        npm = getIntent().getStringExtra("npm");
        comment_counter = getIntent().getIntExtra("comment_counter", 0);

        ThreadJudul = (TextView) findViewById(R.id.detail_judul);
        Nama = (TextView) findViewById(R.id.detail_nama);
        Ket = (TextView) findViewById(R.id.detail_keterangan);
        CC = (TextView) findViewById(R.id.comment_counter);

        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        id_mahasiswa = sharedPreferences.getInt("kirim_id_mahasiswa", 0);

        listview = (ListView) findViewById(R.id.list_komen);
        listview.setDividerHeight(0);

        inputKomen = (EditText) findViewById(R.id.input_komen);

        komen_kosong = (TextView) findViewById(R.id.komen_kosong);

        btnKomen = (ImageView) findViewById(R.id.btnsend);
        btnKomen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });

        inputKomen.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.input_komen){
                    v.getParent().requestDisallowInterceptTouchEvent(true);

                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_UP: v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;

                    }
                }
                return false;
            }
        });

//        detailThread();
    }

    public void komen_list(){
        ThreadJudul.setText(judulThread);
        Nama.setText(namaMahasiswa + ", " + npm + "\nat " + posted);
        Ket.setText("ask \n" + ket);
        CC.setText(String.valueOf(comment_counter));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(DetailThreadActivity.this, null, "Please wait", true, false);

        Call<List<DetailCommentModel>> call = service.getComment(idThread);
        call.enqueue(new Callback<List<DetailCommentModel>>() {
            @Override
            public void onResponse(Call<List<DetailCommentModel>> call, Response<List<DetailCommentModel>> response) {
                data_comment.clear();

                if (response.isSuccessful()){
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        DetailCommentModel data = new DetailCommentModel(
                                response.body().get(i).getId_comment(),
                                response.body().get(i).getComment(),
                                response.body().get(i).getDay(),
                                response.body().get(i).getDate(),
                                response.body().get(i).getTime(),
                                response.body().get(i).getPraktikan(),
                                response.body().get(i).getDosen(),
                                response.body().get(i).getAslab(),
                                response.body().get(i).getId_thread(),
                                response.body().get(i).getStatus(),
                                response.body().get(i).getNama_praktikan(),
                                response.body().get(i).getNpm(),
                                response.body().get(i).getNama_dosen(),
                                response.body().get(i).getNip(),
                                response.body().get(i).getNama_aslab(),
                                response.body().get(i).getUsername()
                        );
                        data_comment.add(data);
                    }

                    adapter = new DetailCommentAdapter(DetailThreadActivity.this, R.layout.item_comment, data_comment);

                    listview.setAdapter(adapter);

                    if (adapter.getCount() < 1){
                        listview.setVisibility(View.INVISIBLE);
                        komen_kosong.setVisibility(View.VISIBLE);
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<DetailCommentModel>> call, Throwable t) {
                Toast.makeText(DetailThreadActivity.this, "Error Retrive Data from Server!!!\n" + t.getMessage(), Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }

    public void send(){
        Retrofit retrofit =new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        Call<DetailCommentModel> call = service.addComent(inputKomen.getText().toString().trim(),
                String.valueOf(id_mahasiswa), idThread);
        call.enqueue(new Callback<DetailCommentModel>() {
            @Override
            public void onResponse(Call<DetailCommentModel> call, Response<DetailCommentModel> response) {
                komen_kosong.setVisibility(View.INVISIBLE);
                listview.setVisibility(View.VISIBLE);

                comment_counter = comment_counter+1;

                komen_list();
                inputKomen.setText("");

            }

            @Override
            public void onFailure(Call<DetailCommentModel> call, Throwable t) {
                Toast.makeText(DetailThreadActivity.this, "Error Store Data from Server!!! \n" + t.getMessage() , Toast.LENGTH_LONG).show();
                loading.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            adapter.clear();
            komen_list();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        data_comment.clear();
        adapter = new DetailCommentAdapter(DetailThreadActivity.this, R.layout.item_comment, data_comment);
        listview.setAdapter(adapter);
        komen_list();
    }

    public void saveData(int poster){
        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("kirim_poster", poster);
        editor.apply();
    }

}
