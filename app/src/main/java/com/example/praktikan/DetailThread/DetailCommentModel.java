package com.example.praktikan.DetailThread;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailCommentModel {
    @SerializedName("id_comment")
    @Expose
    private int id_comment;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("day")
    @Expose
    private String day;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("praktikan")
    @Expose
    private int praktikan;

    @SerializedName("dosen")
    @Expose
    private int dosen;

    @SerializedName("aslab")
    @Expose
    private int aslab;

    @SerializedName("id_thread")
    @Expose
    private int id_thread;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("nama_praktikan")
    @Expose
    private String nama_praktikan;

    @SerializedName("npm")
    @Expose
    private String npm;

    @SerializedName("nama_dosen")
    @Expose
    private String nama_dosen;

    @SerializedName("nip")
    @Expose
    private String nip;

    @SerializedName("nama_aslab")
    @Expose
    private String nama_aslab;

    @SerializedName("username")
    @Expose
    private String username;

    public DetailCommentModel(int id_comment, String comment, String day, String date, String time,
                         int praktikan, int dosen, int aslab, int id_thread, int status,
                         String nama_praktikan, String npm, String nama_dosen,
                         String nip, String nama_aslab, String username){
        this.id_comment = id_comment;
        this.comment = comment;
        this.day = day;
        this.date = date;
        this.time = time;
        this.praktikan = praktikan;
        this.dosen = dosen;
        this.aslab = aslab;
        this.id_thread = id_thread;
        this.status = status;
        this.nama_praktikan = nama_praktikan;
        this.npm = npm;
        this.nama_dosen = nama_dosen;
        this.nip = nip;
        this.nama_aslab = nama_aslab;
        this.username = username;
    }

    public int getId_comment() {
        return id_comment;
    }

    public void setId_comment(int id_comment) {
        this.id_comment = id_comment;
    }

    public int getId_thread() {
        return id_thread;
    }

    public void setId_thread(int id_thread) {
        this.id_thread = id_thread;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPraktikan() {
        return praktikan;
    }

    public void setPraktikan(int praktikan) {
        this.praktikan = praktikan;
    }

    public int getDosen() {
        return dosen;
    }

    public void setDosen(int dosen) {
        this.dosen = dosen;
    }

    public int getAslab() {
        return aslab;
    }

    public void setAslab(int aslab) {
        this.aslab = aslab;
    }

    public String getNama_praktikan() {
        return nama_praktikan;
    }

    public void setNama_praktikan(String nama_praktikan) {
        this.nama_praktikan = nama_praktikan;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getNama_dosen() {
        return nama_dosen;
    }

    public void setNama_dosen(String nama_dosen) {
        this.nama_dosen = nama_dosen;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama_aslab() {
        return nama_aslab;
    }

    public void setNama_aslab(String nama_aslab) {
        this.nama_aslab = nama_aslab;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
