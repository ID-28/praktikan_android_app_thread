package com.example.praktikan.DetailThread;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.praktikan.ApiService;
import com.example.praktikan.CustomOnItemClickListener;
import com.example.praktikan.MainActivity;
import com.example.praktikan.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailCommentAdapter extends ArrayAdapter <DetailCommentModel> {
    private ArrayList<DetailCommentModel> list;
    private LayoutInflater inflater;
    private int res;

    boolean mode;

    int status_thread;

    public DetailCommentAdapter(@NonNull Context context, int resource, @NonNull ArrayList<DetailCommentModel> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        MyHolder holder = null;

        if (convertView == null){
            convertView = inflater.inflate(res, parent, false);
            holder = new MyHolder();

            holder.Komentator = (TextView) convertView.findViewById(R.id.komentar_praktikan);
            holder.Komentator_dosen = (TextView) convertView.findViewById(R.id.komentar_dosen);
            holder.Komentator_aslab = (TextView) convertView.findViewById(R.id.komentar_aslab);
            holder.Komentar = (TextView) convertView.findViewById(R.id.komentar);
            holder.Waktu = (TextView) convertView.findViewById(R.id.waktuKomen);
            holder.Komen = (CardView) convertView.findViewById(R.id.cardview_komen);

            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }

        SharedPreferences sp = ((AppCompatActivity) getContext()).getSharedPreferences("data_user", Context.MODE_PRIVATE);
        int id_login = sp.getInt("kirim_id_mahasiswa", 0);

        SharedPreferences sp_status = ((AppCompatActivity) getContext()).getSharedPreferences("data_status_thread", Context.MODE_PRIVATE);
        status_thread = sp_status.getInt("kirim_status_thread", 0);
        int id_poster = sp_status.getInt("kirim_poster", 0);

        SharedPreferences DarkSP =  ((AppCompatActivity) getContext()).getSharedPreferences("theme_config", 0);
        SharedPreferences.Editor editor = DarkSP.edit();
        mode = DarkSP.getBoolean("nightMode", false);


        if (list.get(position).getPraktikan() != 0){
            holder.Komentator.setVisibility(View.VISIBLE);
            holder.Komentator_dosen.setVisibility(View.GONE);
            holder.Komentator_aslab.setVisibility(View.GONE);
            holder.Komentator.setText(list.get(position).getNama_praktikan() + ", " + list.get(position).getNpm());
//            holder.Komentator.setText(list.get(position).getPraktikan());
        } else if (list.get(position).getDosen() != 0){
            holder.Komentator.setVisibility(View.GONE);
            holder.Komentator_dosen.setVisibility(View.VISIBLE);
            holder.Komentator_aslab.setVisibility(View.GONE);
            holder.Komentator_dosen.setText(list.get(position).getNama_dosen() + ", " + list.get(position).getNip());
//            holder.Komentator_dosen.setText(list.get(position).getDosen());
        } else if (list.get(position).getAslab() != 0){
            holder.Komentator.setVisibility(View.GONE);
            holder.Komentator_dosen.setVisibility(View.GONE);
            holder.Komentator_aslab.setVisibility(View.VISIBLE);
            holder.Komentator_aslab.setText(list.get(position).getNama_aslab() + ", " + list.get(position).getUsername());
//            holder.Komentator_aslab.setText(list.get(position).getAslab());
        } else {
            holder.Komentar.setText(":)");
        }

        holder.Komentar.setText(list.get(position).getComment());

        //time format inside CardView
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);

        Date date = null;
        try {
            date = df.parse(list.get(position).getDate());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Calendar week_before = Calendar.getInstance();
        week_before.setTime(c);
        week_before.add(Calendar.DAY_OF_YEAR, -7);
        Date newDate = week_before.getTime();

        if (list.get(position).getDate().equals(formattedDate)){
            holder.Waktu.setText(list.get(position).getTime());
        } else if (date.before(newDate)){
            holder.Waktu.setText(list.get(position).getDate());
        } else {
            holder.Waktu.setText(list.get(position).getDay());
        }

        //select comment as answer or solution
        if (list.get(position).getStatus() == 0 && id_login == id_poster){
            holder.Komen.setClickable(true);

            holder.Komen.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
                @Override
                public void onItemClicked(View view, int position) {
                    final Context context = view.getContext();

                    new AlertDialog.Builder(context)
                            .setTitle("Konfirmasi")
                            .setMessage("Konfimasi sebagai solusi masalah anda")
                            .setPositiveButton("ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl(MainActivity.BASE_URL)
                                            .addConverterFactory(GsonConverterFactory.create())
                                            .build();

                                    ApiService service = retrofit.create(ApiService.class);

                                    Call<ResponseBody> call = service.updateStatus(String.valueOf(list.get(position).getId_comment()),
                                            String.valueOf(list.get(position).getId_thread()));
                                    call.enqueue(new Callback<ResponseBody>() {
                                        @Override
                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                            SharedPreferences sp_status = ((AppCompatActivity) getContext()).getSharedPreferences("data_status_thread", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sp_status.edit();
                                            editor.putInt("kirim_status_thread", list.get(position).getId_comment());
                                            editor.apply();

                                            Intent intent = ((AppCompatActivity) context).getIntent();

                                            intent.putExtra("id_thread", String.valueOf(list.get(position).getId_thread()));
                                            ((AppCompatActivity) context).startActivity(intent);
                                            ((AppCompatActivity) context).finish();

                                        }

                                        @Override
                                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                                            Toast.makeText(context, "Error" + t.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            })
                            .setNegativeButton("tidak", null)
                            .create().show();
                }
            }));
        } else {
            holder.Komen.setClickable(false);
        }

        //change background colour for the CardView
        if (list.get(position).getId_comment() == status_thread && mode == true) {
            holder.Komen.setCardBackgroundColor(Color.parseColor("#3b66af"));
            holder.Komentator.setTextColor(Color.WHITE);
            holder.Komentator_dosen.setTextColor(Color.WHITE);
            holder.Komentator_aslab.setTextColor(Color.WHITE);
            holder.Komentar.setTextColor(Color.parseColor("#808080"));
            holder.Waktu.setTextColor(Color.parseColor("#808080"));
        } else if (list.get(position).getId_comment() == status_thread && mode == false) {
            holder.Komen.setCardBackgroundColor(Color.parseColor("#3b66af"));
            holder.Komentator.setTextColor(Color.WHITE);
            holder.Komentator_dosen.setTextColor(Color.WHITE);
            holder.Komentator_aslab.setTextColor(Color.WHITE);
            holder.Komentar.setTextColor(Color.WHITE);
            holder.Waktu.setTextColor(Color.WHITE);
        } else if (list.get(position).getId_comment() != status_thread && mode == true){
            holder.Komen.setCardBackgroundColor(Color.parseColor("#29354C"));
            holder.Komentator.setTextColor(Color.WHITE);
            holder.Komentator_dosen.setTextColor(Color.WHITE);
            holder.Komentator_aslab.setTextColor(Color.WHITE);
            holder.Komentar.setTextColor(Color.parseColor("#808080"));
            holder.Waktu.setTextColor(Color.parseColor("#808080"));
        } else {
            holder.Komen.setCardBackgroundColor(Color.WHITE);
            holder.Komentator.setTextColor(Color.BLACK);
            holder.Komentator_dosen.setTextColor(Color.BLACK);
            holder.Komentator_aslab.setTextColor(Color.BLACK);
            holder.Komentar.setTextColor(Color.parseColor("#808080"));
            holder.Waktu.setTextColor(Color.parseColor("#808080"));
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(DetailCommentModel object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder {
        TextView Komentator;
        TextView Komentator_dosen;
        TextView Komentator_aslab;
        TextView Komentar;
        TextView Waktu;

        CardView Komen;
    }
}
