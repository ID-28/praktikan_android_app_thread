package com.example.praktikan;

import com.example.praktikan.DetailThread.DetailCommentModel;
import com.example.praktikan.Materi.MateriModel;
import com.example.praktikan.Modul.ModulModel;
import com.example.praktikan.Praktikum.PraktikumModel;
import com.example.praktikan.ThreadList.ThreadListModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {
    @FormUrlEncoded
    @POST("login-praktikan")
    Call<ModelUser> getLoginData(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("praktikan/thread")
    Call<List<PraktikumModel>> getPraktikum(@Field("username") String username);

    @GET("praktikan/thread/{praktikum}")
    Call<List<ModulModel>> getModul(@Path("praktikum") String praktikum);

    @GET("praktikan/thread/modul/{modul}")
    Call<List<MateriModel>> getMateri(@Path("modul") String modul);

    @GET("praktikan/thread/materi/{materi}")
    Call<List<ThreadListModel>> getThreadList(@Path("materi") String materi);

    @GET("praktikan/thread/detail/{thread}")
    Call<List<DetailCommentModel>> getComment(@Path("thread") String thread);

    @FormUrlEncoded
    @POST("praktikan/comment/create/{thread}")
    Call<DetailCommentModel> addComent(@Field("comment") String comment, @Field("id_mahasiswa") String id_mahasiswa, @Path("thread") String thread);

    //partticipant special feature

    //create new thread
    @FormUrlEncoded
    @POST("praktikan/thread/store")
    Call<ResponseBody> saveThread (@Field("mahasiswa_id") String mahasiswa_id, @Field("materi_id") String materi_id, @Field("judul") String judul, @Field("keterangan") String keterangan);

    //make comment as solution for thread problem
    @FormUrlEncoded
    @POST("praktikan/thread/mark-as-solution/{thread}")
    Call<ResponseBody> updateStatus(@Field("comment_id") String comment_id, @Path("thread") String thread);

}
