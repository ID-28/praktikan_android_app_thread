package com.example.praktikan.Praktikum;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PraktikumModel {
    @SerializedName("namaPraktikum")
    @Expose
    private String namaPraktikum;

    @SerializedName("idPraktikum")
    @Expose
    private int idPraktikum;

    public PraktikumModel(String namaPraktikum, int idPraktikum){
        this.namaPraktikum = namaPraktikum;
        this.idPraktikum = idPraktikum;
    }

    public void setNamaPraktikum(String namaPraktikum) {
        this.namaPraktikum = namaPraktikum;
    }

    public String getNamaPraktikum() {
        return namaPraktikum;
    }

    public void setIdPraktikum(int idPraktikum) {
        this.idPraktikum = idPraktikum;
    }

    public int getIdPraktikum() {
        return idPraktikum;
    }
}
