package com.example.praktikan.Praktikum;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.praktikan.CustomOnItemClickListener;
import com.example.praktikan.Modul.ModulActivity;
import com.example.praktikan.R;

import java.util.ArrayList;

public class PraktikumAdapter extends ArrayAdapter <PraktikumModel> {
    private ArrayList<PraktikumModel> list;
    private LayoutInflater inflater;
    private int res;


    public PraktikumAdapter(@NonNull Context context, int resource, @NonNull ArrayList<PraktikumModel> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent){
        MyHolder holder = null;

        if (convertView == null){
            convertView = inflater.inflate(res, parent, false);

            holder = new MyHolder();

            holder.Name = (TextView) convertView.findViewById(R.id.nama_praktikum);
            holder.Praktikum = (CardView) convertView.findViewById(R.id.cardview_praktikum);

            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }

        holder.Name.setText(list.get(position).getNamaPraktikum());
        holder.Praktikum.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, ModulActivity.class);
                intent.putExtra("id_praktikum", String.valueOf(list.get(position).getIdPraktikum()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));

        return convertView;
    }

    @Override
    public int getCount(){
        return list.size();
    }

    @Override
    public void remove(@Nullable PraktikumModel object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder{
        TextView Name;
        CardView Praktikum;
    }
}
