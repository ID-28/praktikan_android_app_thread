package com.example.praktikan.Praktikum;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.ApiService;
import com.example.praktikan.MainActivity;
import com.example.praktikan.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PraktikumActivity extends AppCompatActivity {
    ArrayList<PraktikumModel> data_praktikum = new ArrayList<PraktikumModel>();
    ListView listView;
    PraktikumAdapter adapter;
    TextView PKosong;

    ProgressDialog loading;
    String sp_username;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_praktikum);

        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        sp_username = sharedPreferences.getString("kirim_username", "");

        PKosong = (TextView) findViewById(R.id.praktikum_kosong);

        listView = (ListView) findViewById(R.id.list_praktikum);
        listView.setDividerHeight(0);
    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(PraktikumActivity.this, null, "Please wait...", true, false);

        Call<List<PraktikumModel>> call = service.getPraktikum(sp_username);
        call.enqueue(new Callback<List<PraktikumModel>>() {
            @Override
            public void onResponse(Call<List<PraktikumModel>> call, Response<List<PraktikumModel>> response) {
                data_praktikum.clear();

                if (response.isSuccessful()) {
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        PraktikumModel data = new PraktikumModel(
                                response.body().get(i).getNamaPraktikum(),
                                response.body().get(i).getIdPraktikum()
                        );
                        data_praktikum.add(data);
                    }

                    adapter = new PraktikumAdapter(PraktikumActivity.this, R.layout.item_praktikum, data_praktikum);
                    listView.setAdapter(adapter);

                    if (adapter.getCount() < 1 ){
                        listView.setVisibility(View.INVISIBLE);
                        PKosong.setVisibility(View.VISIBLE);
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<PraktikumModel>> call, Throwable t) {
                Toast.makeText(PraktikumActivity.this, "Error " + t.getMessage(),Toast.LENGTH_SHORT).show();
                loading.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 ){
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        data_praktikum.clear();
        adapter = new PraktikumAdapter(PraktikumActivity.this, R.layout.item_praktikum, data_praktikum);
        listView.setAdapter(adapter);
        setup();
    }
}
