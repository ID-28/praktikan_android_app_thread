package com.example.praktikan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.Dashboard.Dashboard;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    ArrayList<ModelUser> dataLoginUser = new ArrayList<ModelUser>();

    EditText log_username, log_password;
    Button btn_login;
    CheckBox show_pass;

    ProgressDialog loading;

    int user_id, mahasiswa_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        user_id = sharedPreferences.getInt("kirim_id", 0);
        mahasiswa_id = sharedPreferences.getInt("kirim_id_mahasiswa", 0);

        if (user_id != 0) {
            startActivity(new Intent(LoginActivity.this, Dashboard.class));
            finish();
        }

        log_username = (EditText) findViewById(R.id.textusername);
        log_password = (EditText) findViewById(R.id.textpassword);
        show_pass = (CheckBox) findViewById(R.id.showpass);

        btn_login = (Button) findViewById(R.id.btnlogin);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String e = log_username.getText().toString().trim();
                String p = log_password.getText().toString().trim();

                if (e.equals("")){
                    log_username.setError("Masukkan NPM Anda!!!");
                } else if (p.equals("")){
                    log_password.setError("Masukkan Password Anda!!!");
                } else if (!e.equals("") && !p.equals("")){
                    LoginDataUser(log_username.getText().toString(), log_password.getText().toString());
                }
            }
        });

        show_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (show_pass.isChecked()){
                    log_password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    log_password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
    }

    public void LoginDataUser(final String username, final String password){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(LoginActivity.this, null, "Please wait...", true, false);

        Call<ModelUser> call = service.getLoginData(username, password);
        call.enqueue(new Callback<ModelUser>() {
            @Override
            public void onResponse(Call<ModelUser> call, Response<ModelUser> response) {
                ModelUser modus = response.body();
                assert modus != null;

                if (modus.getStatus() == 200){
                    Intent intent = new Intent(LoginActivity.this, Dashboard.class);
                    startActivity(intent);
                    saveData(modus.getId(), modus.getUsername(), modus.getJenis_user(), modus.getId_mahasiswa());
                    loading.dismiss();
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Email / Password salah !!", Toast.LENGTH_SHORT).show();
                }
                loading.dismiss();
            }

            @Override
            public void onFailure(Call<ModelUser> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Error " + t.getMessage(), Toast.LENGTH_LONG).show();
                loading.dismiss();
            }
        });
    }

    public void saveData(int id, String username, int jenis_user, int id_mahasiswa){
        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("kirim_id", id);
        editor.putString("kirim_username", username);
        editor.putInt("kirim_jenis_user", jenis_user);
        editor.putInt("kirim_id_mahasiswa", id_mahasiswa);
        editor.apply();
    }

    public void login(View view) {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
