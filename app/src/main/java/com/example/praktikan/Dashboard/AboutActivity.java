package com.example.praktikan.Dashboard;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.R;

public class AboutActivity extends AppCompatActivity {

    TextView AboutPage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        AboutPage = (TextView) findViewById(R.id.about_text);
        AboutPage.setText(getString(R.string.about_us));
    }
}
