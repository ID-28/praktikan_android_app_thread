package com.example.praktikan.Dashboard;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.example.praktikan.R;

public class SettingActivity extends AppCompatActivity {

    Switch switch_btn;

    boolean isNightMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        // initializing all our variables.
        switch_btn = (Switch) findViewById(R.id.switch1);

        SharedPreferences DarkSP = getSharedPreferences("theme_config", 0);
        SharedPreferences.Editor editor = DarkSP.edit();
        isNightMode = DarkSP.getBoolean("nightMode", false);

        switch_btn.setChecked(DarkSP.getBoolean("nightMode", false));


        if (isNightMode) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            switch_btn.setChecked(true);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            switch_btn.setChecked(false);
        }

        switch_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked == true) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//                    switch_btn = true;
                    editor.putBoolean("nightMode", true);
                    editor.apply();

                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
//                    isNightMode = false;
                    editor.putBoolean("nightMode", false);
                    editor.apply();
                }
            }
        });
    }
}
