package com.example.praktikan.Dashboard;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.praktikan.Praktikum.PraktikumActivity;
import com.example.praktikan.R;

public class Dashboard extends AppCompatActivity {

    TextView nomp;
    CardView CardForum, CardAbout, CardSetting;

    String npm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        SharedPreferences sharedPreferences = getSharedPreferences("data_user", MODE_PRIVATE);
        npm = sharedPreferences.getString("kirim_username", "");

        nomp = (TextView) findViewById(R.id.npm_dashboard);
        nomp.setText(npm);

        CardForum = (CardView) findViewById(R.id.card_forum);
        CardForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, PraktikumActivity.class);
                startActivity(intent);
            }
        });

        CardAbout = (CardView) findViewById(R.id.card_about);
        CardAbout.setOnClickListener(v -> {
            Intent intent = new Intent(Dashboard.this, AboutActivity.class);
            startActivity(intent);
        });

        CardSetting = (CardView) findViewById(R.id.card_setting);
        CardSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, SettingActivity.class);
                startActivity(intent);
            }
        });
    }
}
