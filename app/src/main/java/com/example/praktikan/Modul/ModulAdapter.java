package com.example.praktikan.Modul;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.praktikan.CustomOnItemClickListener;
import com.example.praktikan.Materi.MateriActivity;
import com.example.praktikan.R;

import java.util.ArrayList;

public class ModulAdapter extends ArrayAdapter<ModulModel> {

    private ArrayList<ModulModel> list;
    private LayoutInflater inflater;
    private int res;

    public ModulAdapter(@NonNull Context context, int resource, @NonNull ArrayList<ModulModel> list) {
        super(context, resource, list);
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ModulAdapter.MyHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(res, parent, false);

            holder = new ModulAdapter.MyHolder();

            holder.ModulName = (TextView) convertView.findViewById(R.id.nama_modul);
            holder.Modul = (CardView) convertView.findViewById(R.id.cardview_modul);

            convertView.setTag(holder);
        } else {
            holder = (ModulAdapter.MyHolder) convertView.getTag();
        }

        holder.ModulName.setText(list.get(position).getNamaModul());
        holder.Modul.setOnClickListener(new CustomOnItemClickListener(position, new CustomOnItemClickListener.OnItemClickCallback() {
            @Override
            public void onItemClicked(View view, int position) {
                final Context context = view.getContext();

                Intent intent = new Intent(context, MateriActivity.class);
                intent.putExtra("id_modul", String.valueOf(list.get(position).getIdModul()));
                ((AppCompatActivity) context).startActivity(intent);
            }
        }));

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void remove(ModulModel object) {
        super.remove(object);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class MyHolder {
        TextView ModulName;
        CardView Modul;
    }
}
