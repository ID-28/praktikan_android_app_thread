package com.example.praktikan.Modul;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.praktikan.ApiService;
import com.example.praktikan.MainActivity;
import com.example.praktikan.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModulActivity extends AppCompatActivity {

    ArrayList<ModulModel> data_modul = new ArrayList<ModulModel>();
    ListView listview;
    ModulAdapter adapter;

    TextView ModKosong;

    ProgressDialog loading;

    String id_praktikum;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modul);

        id_praktikum = getIntent().getStringExtra("id_praktikum");

        listview = (ListView) findViewById(R.id.list_modul);
        listview.setDividerHeight(0);
    }

    public void setup(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        loading = ProgressDialog.show(ModulActivity.this, null, "Please wait...", true, false);

        Call<List<ModulModel>> call = service.getModul(id_praktikum);
        call.enqueue(new Callback<List<ModulModel>>() {
            @Override
            public void onResponse(Call<List<ModulModel>> call, Response<List<ModulModel>> response) {
                data_modul.clear();

                if (response.isSuccessful()){
                    int jumlah = response.body().size();

                    for (int i = 0; i < jumlah; i++){
                        ModulModel data = new ModulModel(
                                response.body().get(i).getNamaModul(),
                                response.body().get(i).getIdModul()
                        );

                        data_modul.add(data);
                    }

                    adapter = new ModulAdapter(ModulActivity.this, R.layout.item_modul, data_modul);
                    listview.setAdapter(adapter);

                    if (adapter.getCount() < 1 ){
                        listview.setVisibility(View.INVISIBLE);
                        ModKosong.setVisibility(View.VISIBLE);
                    }

                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<ModulModel>> call, Throwable t) {
                Toast.makeText(ModulActivity.this, "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1){
            adapter.clear();
            setup();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        data_modul.clear();
        adapter = new ModulAdapter(ModulActivity.this, R.layout.item_modul, data_modul);
        listview.setAdapter(adapter);
        setup();
    }
}
